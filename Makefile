CONTAINER=cj_oster_resume
VERSION=$(shell cat VERSION)
SOURCES=$(wildcard src/*)
PRIVATE=$(wildcard private/*)
DOCKER=$(shell type -P podman || type -P docker)

all: pdf

pdf: $(SOURCES) container VERSION private_test
	$(DOCKER) run --rm=true -v $$(pwd):/tmp/resume:rw,Z $(CONTAINER) make
	cp src/*$(VERSION).pdf ./

preview: pdf
	type -P evince > /dev/null && (evince *_$(VERSION).pdf > /dev/null 2>&1 & true) || (type -P atril > /dev/null && (atril *_$(VERSION).pdf > /dev/null 2>&1& true))

container: 
	$(DOCKER) build --rm=true -t $(CONTAINER) .

VERSION: version
	ln -sf src/VERSION ./VERSION

version:
	[ -z "$$(git for-each-ref --sort=creatordate --format '%(refname) %(creatordate)' refs/tags | cut -d" " -f 1 | cut -d/ -f 3 | tail -n 1)" ] || \
		git for-each-ref --sort=creatordate --format '%(refname) %(creatordate)' refs/tags | cut -d" " -f 1 | cut -d/ -f 3 | tail -n 1 | sed 's/^v//' > src/VERSION

clean: private.asc
	! $(DOCKER) images | tail -n +2 | grep -E "^[[:graph:]]*/?$(CONTAINER)[[:space:]]" || { $(DOCKER) run --rm -v $$(pwd):/tmp/resume:rw,Z $(CONTAINER) make clean; $(DOCKER) rmi $(CONTAINER); }
	rm -rf private

private_test: private.asc private
	[ ! -z $${PRIVATE_INFO} ] || rm -rf private

private:
	[ -z $${PRIVATE_INFO} ] || gpg2 -d private.asc | tar -xJvvf -

private.asc: $(PRIVATE)
	tar -cJvvf - private | gpg2 -aer cj@cjoster.com -r ocj@vmware.com > private.asc.tmp && mv private.asc.tmp private.asc
	rm -f private.asc.tmp

distclean: clean
	rm -f *.pdf
	rm -f src/version VERSION

# dist: resume.pdf VERSION
#         cp resume.pdf ../$(RES_OUTPUT)-`cat VERSION`.pdf
#         make clean
# 
# sync: dist
#         tar --exclude \*.tar.bz2 --exclude \*.bak -cjvvf ../$(RES_OUTPUT)-`cat VERSION`.tar.bz2 ../
#         cat ../$(RES_OUTPUT)-`cat VERSION`.tar.bz2 | ssh lordvadr@cjoster.com "cat > /www/cjoster.com/html/$(RES_OUTPUT)-`cat VERSION`.tar.bz2; rm -f /www/cjoster.com/html/$(RES_OUTPUT).tar.bz2; ln -s /www/cjoster.com/html/$(RES_OUTPUT)-`cat VERSION`.tar.bz2 /www/cjoster.com/html/$(RES_OUTPUT).tar.bz2"
# 
# unsync:
# 	ssh lordvadr@cjoster.com "rm -f /www/cjoster.com/html/$(RES_OUTPUT)*"

	
