FROM fedora:35

RUN dnf --setopt=tsflags=nodocs -y install texlive-latex-bin-bin make \
    texlive-fancyhdr texlive-metafont-bin texlive-ncntrsbk texlive-gsftopk \
    texlive-dvips poppler-utils

WORKDIR /tmp/resume/src
